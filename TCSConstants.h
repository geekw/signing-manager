//
//  TCSConstants.h
//  Identity Manager
//
//  Created by Timothy Perfitt on 11/18/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.
//

#ifndef TCSConstants_h
#define TCSConstants_h
#define TCSCARDINSERTIONCHANGED @"TCSCARDINSERTIONCHANGED"
#define TOUCHCARDPATH @"/tmp/scinsert"
#define TCSSMARTCARDCHANGED @"TCSSmartCardChanged"
#define SUPPORTURLS [[[NSBundle mainBundle] infoDictionary] objectForKey:@"Support URLs"]

#define TCSSMARTCARDTOKENCHANGED @"TCSSmartCardTokenChanged"

#define REFRESHKEY @"refreshkey"

#define TCSKEYCHAINSERVICE @"com.twocanoes.signing-manager"
#define TCSKEYCHAINACCOUNT @"default"
#define TCSKEYCHAINACCESSGROUP @"UXP6YEHSPW.com.twocanoes.signing-manager"

#define TCSAPPGROUPIDENTIFIER @"UXP6YEHSPW.com.twocanoes.signing-manager"
#endif /* TCSConstants_h */
