//
//  Identity_ManagerTests.m
//  Identity ManagerTests
//
//  Created by Timothy Perfitt on 12/29/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSData+SHA1.h"
@interface Identity_ManagerTests : XCTestCase

@end

@implementation Identity_ManagerTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testSha1 {


    char bytes[]={ 0x30, 0x82, 0x04, 0x0d, 0x30, 0x82, 0x02, 0xf5, 0xa0, 0x03, 0x02, 0x01,
         0x02, 0x02, 0x06, 0x01, 0x6f, 0x4f, 0xd2, 0x2c, 0x3e, 0x30, 0x0d, 0x06,
         0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0b, 0x05, 0x00,
         0x30, 0x81, 0x8b, 0x31, 0x0b, 0x30, 0x09, 0x06, 0x03, 0x55, 0x04, 0x06,
         0x13, 0x02, 0x55, 0x53, 0x31, 0x1b, 0x30, 0x19, 0x06, 0x03, 0x55, 0x04,
         0x0a, 0x0c, 0x12, 0x54, 0x77, 0x6f, 0x63, 0x61, 0x6e, 0x6f, 0x65, 0x73,
         0x20, 0x53, 0x6f, 0x66, 0x74, 0x77, 0x61, 0x72, 0x65, 0x31, 0x13, 0x30,
         0x11, 0x06, 0x03, 0x55, 0x04, 0x0b, 0x0c, 0x0a, 0x55, 0x58, 0x50, 0x36,
         0x59, 0x45, 0x48, 0x53, 0x53, 0x53, 0x31, 0x4a, 0x30, 0x48, 0x06, 0x03,
         0x55, 0x04, 0x03, 0x0c, 0x41, 0x54, 0x65, 0x73, 0x74, 0x20, 0x53, 0x6f,
         0x66, 0x74, 0x77, 0x61, 0x72, 0x65, 0x20, 0x44, 0x69, 0x73, 0x74, 0x72,
         0x69, 0x62, 0x75, 0x74, 0x69, 0x6f, 0x6e, 0x3a, 0x20, 0x54, 0x77, 0x6f,
         0x63, 0x61, 0x6e, 0x6f, 0x65, 0x73, 0x20, 0x53, 0x6f, 0x66, 0x74, 0x77,
         0x61, 0x72, 0x65, 0x2c, 0x20, 0x49, 0x6e, 0x63, 0x2e, 0x20, 0x28, 0x55,
         0x58, 0x50, 0x36, 0x59, 0x45, 0x48, 0x53, 0x53, 0x53, 0x29, 0x30, 0x1e,
         0x17, 0x0d, 0x31, 0x39, 0x31, 0x32, 0x32, 0x39, 0x30, 0x34, 0x30, 0x32,
         0x35, 0x31, 0x5a, 0x17, 0x0d, 0x32, 0x30, 0x31, 0x32, 0x32, 0x38, 0x30,
         0x34, 0x30, 0x32, 0x35, 0x32, 0x5a, 0x30, 0x81, 0x8b, 0x31, 0x0b, 0x30,
         0x09, 0x06, 0x03, 0x55, 0x04, 0x06, 0x13, 0x02, 0x55, 0x53, 0x31, 0x1b,
         0x30, 0x19, 0x06, 0x03, 0x55, 0x04, 0x0a, 0x0c, 0x12, 0x54, 0x77, 0x6f,
         0x63, 0x61, 0x6e, 0x6f, 0x65, 0x73, 0x20, 0x53, 0x6f, 0x66, 0x74, 0x77,
         0x61, 0x72, 0x65, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x0b,
         0x0c, 0x0a, 0x55, 0x58, 0x50, 0x36, 0x59, 0x45, 0x48, 0x53, 0x53, 0x53,
         0x31, 0x4a, 0x30, 0x48, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x41, 0x54,
         0x65, 0x73, 0x74, 0x20, 0x53, 0x6f, 0x66, 0x74, 0x77, 0x61, 0x72, 0x65,
         0x20, 0x44, 0x69, 0x73, 0x74, 0x72, 0x69, 0x62, 0x75, 0x74, 0x69, 0x6f,
         0x6e, 0x3a, 0x20, 0x54, 0x77, 0x6f, 0x63, 0x61, 0x6e, 0x6f, 0x65, 0x73,
         0x20, 0x53, 0x6f, 0x66, 0x74, 0x77, 0x61, 0x72, 0x65, 0x2c, 0x20, 0x49,
         0x6e, 0x63, 0x2e, 0x20, 0x28, 0x55, 0x58, 0x50, 0x36, 0x59, 0x45, 0x48,
         0x53, 0x53, 0x53, 0x29, 0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09,
         0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03,
         0x82, 0x01, 0x0f, 0x00, 0x30, 0x82, 0x01, 0x0a, 0x02, 0x82, 0x01, 0x01,
         0x00, 0x94, 0x28, 0x42, 0x0c, 0x96, 0xc2, 0x62, 0xd2, 0xb5, 0x08, 0x05,
         0xe7, 0xb8, 0x74, 0x40, 0x66, 0x98, 0xbb, 0xbc, 0x26, 0x4f, 0x11, 0xd6,
         0x31, 0x3d, 0xda, 0x86, 0xd3, 0xb1, 0x32, 0xac, 0xf8, 0xe9, 0x76, 0x6f,
         0x01, 0x47, 0x67, 0xd5, 0x34, 0x32, 0xef, 0x72, 0x97, 0xbf, 0xd4, 0xaf,
         0x5c, 0x8b, 0xf3, 0x18, 0xb1, 0xca, 0x2a, 0x2b, 0xd8, 0x2b, 0x26, 0xf1,
         0x50, 0x31, 0x92, 0xf6, 0xde, 0xab, 0xe4, 0x2a, 0x8d, 0xd2, 0x54, 0x0d,
         0x82, 0xe5, 0x70, 0x6c, 0xbd, 0xea, 0xba, 0xf6, 0xba, 0x7a, 0xcd, 0x61,
         0x18, 0x88, 0x6c, 0xe7, 0x1c, 0xcd, 0x5b, 0xf7, 0xb6, 0x25, 0x6b, 0x98,
         0x10, 0xb6, 0xd3, 0xf7, 0x6d, 0xc6, 0x07, 0xb7, 0x19, 0x9d, 0x9f, 0x04,
         0xf1, 0x48, 0xdc, 0x4b, 0x92, 0x05, 0xd8, 0x81, 0x8d, 0x27, 0x7e, 0x19,
         0x52, 0xfd, 0x08, 0xf8, 0x9c, 0xc5, 0xa0, 0x52, 0xdb, 0x49, 0x27, 0xbd,
         0x32, 0x5f, 0x43, 0x9a, 0xa0, 0x57, 0x82, 0x50, 0x19, 0x41, 0x81, 0x92,
         0x25, 0x27, 0x27, 0x1e, 0x52, 0xed, 0x85, 0x38, 0x0e, 0x9d, 0xa3, 0xe1,
         0x25, 0x64, 0xb9, 0xca, 0x4c, 0xef, 0x0e, 0x5e, 0xea, 0x0e, 0xe9, 0x78,
         0xaf, 0x4f, 0x70, 0xad, 0xce, 0x95, 0x32, 0x75, 0x31, 0xfd, 0x86, 0xbf,
         0x0d, 0x1b, 0xfe, 0x7b, 0xf1, 0x3a, 0x0b, 0xab, 0xe1, 0x82, 0x86, 0x1e,
         0xe9, 0xac, 0x36, 0xc5, 0xd0, 0x57, 0x0e, 0xc2, 0x08, 0x5f, 0xc2, 0x1a,
         0xd7, 0x2e, 0xea, 0x0f, 0x59, 0xfa, 0x49, 0x06, 0x49, 0x36, 0x56, 0x92,
         0x48, 0xbb, 0x39, 0x91, 0xa0, 0x31, 0x61, 0x3a, 0x36, 0x01, 0x73, 0x75,
         0xec, 0xfe, 0xd2, 0xef, 0x29, 0x6d, 0xe9, 0x49, 0x3a, 0xb1, 0xde, 0x07,
         0xb1, 0x97, 0xe3, 0xd4, 0x26, 0xe6, 0xd6, 0x9d, 0x68, 0xba, 0xef, 0x71,
         0x11, 0x94, 0xd8, 0x70, 0x47, 0x02, 0x03, 0x01, 0x00, 0x01, 0xa3, 0x75,
         0x30, 0x73, 0x30, 0x1f, 0x06, 0x03, 0x55, 0x1d, 0x23, 0x04, 0x18, 0x30,
         0x16, 0x80, 0x14, 0x4e, 0x8b, 0x43, 0x7f, 0x1f, 0xd4, 0xa8, 0xbc, 0xcd,
         0xe5, 0xea, 0xaf, 0x3f, 0xf7, 0x3c, 0x06, 0x23, 0x1e, 0xc6, 0xbf, 0x30,
         0x0c, 0x06, 0x03, 0x55, 0x1d, 0x13, 0x01, 0x01, 0xff, 0x04, 0x02, 0x30,
         0x00, 0x30, 0x0e, 0x06, 0x03, 0x55, 0x1d, 0x0f, 0x01, 0x01, 0xff, 0x04,
         0x04, 0x03, 0x02, 0x05, 0xa0, 0x30, 0x1d, 0x06, 0x03, 0x55, 0x1d, 0x0e,
         0x04, 0x16, 0x04, 0x14, 0x4e, 0x8b, 0x43, 0x7f, 0x1f, 0xd4, 0xa8, 0xbc,
         0xcd, 0xe5, 0xea, 0xaf, 0x3f, 0xf7, 0x3c, 0x06, 0x23, 0x1e, 0xc6, 0xbf,
         0x30, 0x13, 0x06, 0x03, 0x55, 0x1d, 0x25, 0x04, 0x0c, 0x30, 0x0a, 0x06,
         0x08, 0x2b, 0x06, 0x01, 0x05, 0x05, 0x07, 0x03, 0x03, 0x30, 0x0d, 0x06,
         0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0b, 0x05, 0x00,
         0x03, 0x82, 0x01, 0x01, 0x00, 0x19, 0x8b, 0x0e, 0x72, 0x01, 0x82, 0x5b,
         0x5a, 0xec, 0xd4, 0x22, 0x66, 0x30, 0x57, 0x0d, 0x69, 0xc2, 0x86, 0x9d,
         0xb8, 0xcc, 0xda, 0x94, 0xa7, 0x32, 0x8f, 0x8d, 0xf1, 0xae, 0x85, 0x30,
         0x7b, 0xae, 0xcc, 0x94, 0x7d, 0x83, 0x2a, 0x54, 0xd5, 0x88, 0x1f, 0xfc,
         0x04, 0x6f, 0x57, 0x46, 0xbe, 0xa0, 0xef, 0x0c, 0xce, 0x77, 0x81, 0xd0,
         0x65, 0xc9, 0x0d, 0xea, 0x57, 0xb0, 0xe8, 0xb3, 0x1a, 0xf7, 0x01, 0xe4,
         0x3a, 0x4b, 0x25, 0x20, 0x9a, 0x2b, 0x16, 0x97, 0x05, 0x17, 0x47, 0x3e,
         0xc5, 0x56, 0xbb, 0x4f, 0xa5, 0x08, 0xf8, 0xc8, 0x65, 0xe3, 0x76, 0x48,
         0x8a, 0x9a, 0xec, 0x77, 0x35, 0x75, 0xf2, 0xe6, 0x22, 0x18, 0xd8, 0x8f,
         0x89, 0xa4, 0x18, 0xf5, 0xac, 0xd3, 0x61, 0xa0, 0x5a, 0xf1, 0x3a, 0xfd,
         0xa7, 0x2c, 0xd0, 0x9b, 0xa0, 0x6e, 0x25, 0x81, 0x58, 0xc5, 0x70, 0x2b,
         0xdb, 0xfc, 0x17, 0x8c, 0x69, 0x39, 0x2c, 0x01, 0xd0, 0x49, 0x1f, 0x18,
         0xf8, 0x67, 0x65, 0xe7, 0x88, 0x83, 0xa0, 0x38, 0x6b, 0xa1, 0x88, 0x18,
         0x46, 0x4d, 0x39, 0x17, 0x97, 0xf3, 0x37, 0x49, 0x5d, 0x55, 0x85, 0x05,
         0xff, 0x0b, 0xb2, 0xe5, 0x1b, 0xea, 0xb9, 0xaf, 0xd5, 0x4b, 0xd5, 0x53,
         0xf4, 0x0d, 0xd3, 0xd4, 0xd1, 0x5c, 0xdd, 0x7b, 0x52, 0x62, 0xa2, 0x1e,
         0x3a, 0xa5, 0xd0, 0xa5, 0x8e, 0x67, 0x5f, 0xc7, 0x02, 0x6b, 0xd1, 0x67,
         0x4b, 0x8a, 0x61, 0x41, 0xce, 0x77, 0x28, 0x1a, 0x07, 0x23, 0x5a, 0x42,
         0x4e, 0x32, 0xc4, 0xc6, 0x3a, 0xb3, 0xdc, 0x54, 0x10, 0xa1, 0x84, 0xa1,
         0x31, 0xb3, 0x07, 0xac, 0x56, 0xbf, 0x09, 0xce, 0x67, 0xd1, 0x54, 0xd0,
         0x8a, 0xb0, 0xee, 0xa0, 0x1d, 0xcf, 0x43, 0xf1, 0xc0, 0xb3, 0x95, 0x05,
        0x56, 0xc6, 0xb7, 0x63, 0x4e, 0xdd, 0x93, 0x8b, 0x9f};
    NSData *testCert=[NSData dataWithBytes:bytes length:sizeof(bytes)];

    NSData *hash=[testCert sha1];

    char expectedBytes[]={0x7b, 0xac, 0xa7, 0xc5, 0xe1, 0x22, 0xe7, 0x15, 0x88, 0x29, 0x35, 0xb3,
        0xb9, 0xb4, 0xf5, 0x24, 0x3a, 0xf2, 0xad, 0xa4};
    NSData *expectedHash=[NSData dataWithBytes:expectedBytes length:sizeof(expectedBytes)];

    XCTAssertTrue([expectedHash isEqualToData:hash]);
}



@end
