# README #

Signing Manager for macOS provides an ideal system for securing the code and package signing identities. Implement secure access to the identities with API keys, access controls, and auditing. Gain full control over iOS, macOS, watchOS, and iPad OS signing operations without disrupting the current workflow. Works great for developers, CI/CD servers, or for any size team that releases software on Apple hardware.

### What Signing Manager For? ###

* Sign apps and packages without direct access to private keys
* Local signing with native Apple tools
* Limit signing operations based on API key
* Signing Service on Linux, macOS or Windows
* Full auditing of all signing operations
* Easy updating of expired certificates
* Command line interface for CI/CD servers

### More Information ###

Follow [@tperfitt](https://twitter.com/tperfitt) on Twitter.

Join [MacAdmins Slack](http://macadmins.org/) and checkout the [#twocanoes-signing-manager](https://macadmins.slack.com/archives/CQVUG1FPZ) channel.
