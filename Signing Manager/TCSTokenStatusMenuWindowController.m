//
//  TCSTokenStatusMenuWindowController.m
//  Identity Manager
//
//  Created by Timothy Perfitt on 11/18/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.//

#import "TCSTokenStatusMenuWindowController.h"
#import "TCSIdentityManager.h"
#import "TCSConstants.h"
#import "NSData+SHA1.h"
#import "NSData+HexString.h"
#import "TCSUnifiedLogger.h"
#import "AppDelegate.h"
#import "TCSKeychain.h"
#import "TCTaskWrapperWithBlocks.h"
@interface TCSTokenStatusMenuWindowController ()
@property (weak) IBOutlet NSTableView *certificateTableView;
@property (strong) IBOutlet NSPopover *popover;
@property (strong) IBOutlet NSMenu *commandsMenu;
@property (strong) NSString *statusText;
@property (strong) id cardInsertNotification;
@property (strong) NSArray *certificateArray;
@property (assign) id doubleClick;
@property (weak) id buttonCNTarget;
@property (assign) BOOL isRefreshing;
@property (strong) NSTimer *refreshTimer;
@property (strong) TCTaskWrapperWithBlocks *tw;
@property (strong) IBOutlet NSArrayController *certificateArrayController;
@end

@implementation TCSTokenStatusMenuWindowController


-(void)awakeFromNib{
    static BOOL loaded=NO;
    if (loaded==NO){

        NSSortDescriptor *sourceSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"source" ascending:NO];
                NSSortDescriptor *labelSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
        [self.certificateArrayController setSortDescriptors:@[sourceSortDescriptor,labelSortDescriptor]];


        [self.certificateTableView registerForDraggedTypes:[NSArray arrayWithObjects: NSFilenamesPboardType, nil]];
        loaded=YES;

        self.doubleClick=self;
        self.buttonCNTarget=self;

        [[NSNotificationCenter defaultCenter] addObserverForName:TCSSMARTCARDCHANGED object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self loadCertificates:self];
            });

        }];


        NSString *apiKey=[TCSKeychain passwordForService:TCSKEYCHAINSERVICE account:TCSKEYCHAINACCOUNT
                                             accessGroup:TCSKEYCHAINACCESSGROUP error:nil];
        if([[TCSPreferenceController sharedController] apiHost] && apiKey) {

            [self refresh:self];
        }
        else {
            AppDelegate *appDelegate=(AppDelegate *)[NSApp delegate];
            [appDelegate performSelector:@selector(showPreferences:) withObject:self afterDelay:1];

        }
    }

}

-(void)showCertificateWindow:(id)sender{

    [self.window makeKeyAndOrderFront:self];

}
- (void)mouseDown:(NSEvent *)event{

}
-(IBAction)showCurrentlySelectedCertificate:(id)sender{

    NSDictionary *current=[[self.certificateArrayController selectedObjects] firstObject];
    SecCertificateRef certRef=(__bridge SecCertificateRef)[current objectForKey:@"certificate"];


    if (!certRef) return;
    [self.certificateInterfaceDelegate showCertificate:(__bridge id _Nonnull)(certRef)];
}
-(void)certificateViewDoubleClick:(NSOutlineView *)outlineView{

    [self showCurrentlySelectedCertificate:self];



}
- (IBAction)copyHash:(id)sender {
    NSDictionary *current=[[self.certificateArrayController selectedObjects] firstObject];
    NSString *hash=[current objectForKey:@"hash"];
    [[NSPasteboard generalPasteboard] clearContents];

    [[NSPasteboard generalPasteboard] setString:hash forType:NSPasteboardTypeString];

}
- (IBAction)copyCodesignCommandMenuSelected:(id)sender {
    if (self.certificateTableView.clickedRow>=0){
        [self copyCodesignCommandAtIndex:self.certificateTableView.clickedRow];
    }

}
- (IBAction)copySelectedCodesignCommand:(id)sender {
    NSInteger selectedIndex=[self.certificateArrayController selectionIndex];
    if (selectedIndex>=0){
        [self copyCodesignCommandAtIndex:selectedIndex];
    }

}
-(void)copyCodesignCommandAtIndex:(NSInteger)index{
    NSDictionary *current=[[self.certificateArrayController arrangedObjects] objectAtIndex:index];
    NSString *hash=[current objectForKey:@"hash"];

    NSString *command=[NSString stringWithFormat:@"codesign -fs \"%@\" ",hash];
    [[NSPasteboard generalPasteboard] clearContents];
    [[NSPasteboard generalPasteboard] setString:command forType:NSPasteboardTypeString];

}
- (IBAction)copySelectedProductSign:(id)sender {
    NSInteger selectedIndex=[self.certificateArrayController selectionIndex];
    if (selectedIndex>=0){
        [self copyProductSignCommandAtIndex:selectedIndex];
    }

}
-(void)copyProductSignCommandAtIndex:(NSInteger)index{
    NSDictionary *current=[[self.certificateArrayController arrangedObjects] objectAtIndex:index];
    NSString *hash=[current objectForKey:@"hash"];

    NSString *command=[NSString stringWithFormat:@"productsign --sign \"%@\" source.pkg destination.pkg",hash];
    [[NSPasteboard generalPasteboard] clearContents];
    [[NSPasteboard generalPasteboard] setString:command forType:NSPasteboardTypeString];


}
- (IBAction)copyProductSignCommandMenuSelected:(id)sender {
    if (self.certificateTableView.clickedRow>=0){
        [self copyProductSignCommandAtIndex:self.certificateTableView.clickedRow];
    }

}
- (IBAction)copySHA1HashMenuItemSelected:(id)sender {
    if (self.certificateTableView.clickedRow>=0){
        NSDictionary *current=[[self.certificateArrayController arrangedObjects] objectAtIndex:self.certificateTableView.clickedRow];
        NSString *hash=[current objectForKey:@"hash"];
        [[NSPasteboard generalPasteboard] clearContents];

        [[NSPasteboard generalPasteboard] setString:hash forType:NSPasteboardTypeString];
    }

}
- (IBAction)copyCommonNameMenuItemSelected:(id)sender {
    if (self.certificateTableView.clickedRow>=0){
        NSDictionary *current=[[self.certificateArrayController arrangedObjects] objectAtIndex:self.certificateTableView.clickedRow];
        NSString *label=[current objectForKey:@"label"];

        [[NSPasteboard generalPasteboard] clearContents];
        [[NSPasteboard generalPasteboard] setString:label forType:NSPasteboardTypeString];
    }

    
}
-(NSString *)sha1HashAtIndex:(NSInteger) index{
    if (index>=0){
        NSDictionary *current=[[self.certificateArrayController arrangedObjects] objectAtIndex:index];
        return [current objectForKey:@"hash"];
    }
    return nil;
}
-(NSString *)sha1Hash{
    NSInteger selectedIndex=[self.certificateArrayController selectionIndex];
    if (selectedIndex>=0){
        NSDictionary *current=[[self.certificateArrayController arrangedObjects] objectAtIndex:selectedIndex];
        return [current objectForKey:@"hash"];
    }
    return nil;
}
- (IBAction)copySHA1Hash:(id)sender {

    NSString *hash=[self sha1Hash];
    if (hash){
        [[NSPasteboard generalPasteboard] clearContents];
        [[NSPasteboard generalPasteboard] setString:hash forType:NSPasteboardTypeString];
    }


}
- (NSString *)copyCNAtIndex:(NSInteger )index
{
    if (index>=0){
        NSDictionary *current=[[self.certificateArrayController arrangedObjects] objectAtIndex:index];
        return [current objectForKey:@"label"];
    }
    return nil;


}
- (IBAction)copyCN:(id)sender {
    NSDictionary *current=[[self.certificateArrayController selectedObjects] firstObject];
    NSString *label=[current objectForKey:@"label"];

    [[NSPasteboard generalPasteboard] clearContents];
    [[NSPasteboard generalPasteboard] setString:label forType:NSPasteboardTypeString];

}

- (IBAction)commandsButtonPressed:(NSButton *)button {

    [self.commandsMenu popUpMenuPositioningItem:nil atLocation:[NSEvent mouseLocation] inView:nil];
}

-(void)loadCertificates:(id)sender{
    self.certificateArray=[[TCSIdentityManager manager] certificates];

    if (self.certificateArray.count>0){
        [self.refreshTimer invalidate];
        self.isRefreshing=NO;
    }
}
-(void)timeoutError:(id)sender{
    //    self.isRefreshing=NO;
    //    NSAlert *alert=[[NSAlert alloc] init];
    //    alert.messageText=@"Refresh Error";
    //    alert.informativeText=@"There was an error refreshing certificates. Please check your network connection and settings and try again.";
    //
    //    [alert addButtonWithTitle:@"OK"];
    //    [alert addButtonWithTitle:@"Show Log"];
    //
    //
    //
    //    NSInteger res=[alert runModal];
    //
    //    if (res==NSAlertSecondButtonReturn) {
    //
    //        AppDelegate *appDelegate=(AppDelegate *)[NSApp delegate];
    //
    //        [appDelegate showLog:self];
    //
    //        #pragma clang diagnostic pop
    //
    //    }

}
-(IBAction)refresh:(id)sender {
    self.isRefreshing=YES;
    if(self.refreshTimer) [self.refreshTimer invalidate];
    self.refreshTimer=[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timeoutError:) userInfo:nil repeats:NO];
    [[TCSIdentityManager manager] toggleCard:self];
}

- (void)windowDidLoad {
    [super windowDidLoad];

}

-(void)dealloc{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self.cardInsertNotification];
    self.cardInsertNotification=nil;
}

-(void)updateStatus:(id)sender{
    if ([[TCSIdentityManager manager] isInserted:self]){

        self.statusText=@"";
    }
    else{
        self.statusText=@"";
    }
}


-(void)sign:(NSString *)inPath withSHA1Hash:(NSString *)inHash{

    BOOL signApp=YES;
    NSURL *saveURL;
    if ([[[inPath pathExtension] lowercaseString] isEqualToString:@"pkg"]){

        NSSavePanel *savePanel=[NSSavePanel savePanel];
        savePanel.allowsOtherFileTypes=NO;
        savePanel.allowedFileTypes=@[@"pkg"];
        savePanel.message=@"Select a file and location to save:";
        NSModalResponse res=[savePanel runModal];

        if (res!=NSModalResponseOK) {

            return;
        }

        saveURL=savePanel.URL;
        signApp=NO;
    }
    NSError *err;
    NSURL* scriptsDirURL = [[NSFileManager defaultManager] URLForDirectory: NSApplicationScriptsDirectory inDomain: NSUserDomainMask appropriateForURL: nil create: YES error: &err];

    NSURL *scriptURL=[scriptsDirURL URLByAppendingPathComponent:@"sign.sh"];

    NSUserUnixTask *unixTask=[[NSUserUnixTask alloc] initWithURL:scriptURL error:&err];

    NSPipe *outPipe=[NSPipe pipe];
    NSPipe *stdErrPipe=[NSPipe pipe];

    unixTask.standardOutput=outPipe.fileHandleForWriting;
    unixTask.standardError=stdErrPipe.fileHandleForWriting;

    NSArray *args;
    if (signApp==YES){
        args=@[inHash,inPath];
    }
    else{
        args=@[inHash,inPath,saveURL.path];

    }
    [unixTask executeWithArguments:args completionHandler:^(NSError * _Nullable error) {

        NSData *outputData=[outPipe.fileHandleForReading readDataToEndOfFile];
        NSString *outputString=[[NSString alloc] initWithData:outputData encoding:NSUTF8StringEncoding];

        NSLog(@"%@",outputString);
        NSData *errOutputData=[stdErrPipe.fileHandleForReading readDataToEndOfFile];
        NSString *errOutputString=[[NSString alloc] initWithData:errOutputData encoding:NSUTF8StringEncoding];


        dispatch_async(dispatch_get_main_queue(), ^{

            if (error!=nil) {
                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Signing Failed";
                alert.informativeText=[NSString stringWithFormat:@"Failed signing application: %@. Error: %@",inPath,errOutputString    ];

                [alert addButtonWithTitle:@"OK"];
                [alert runModal];
            }
            else {

                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Signing Successful";
                alert.informativeText=[NSString stringWithFormat:@"The application %@ was signed successfully",inPath];

                [alert addButtonWithTitle:@"OK"];
                [alert runModal];

            }
        });
    }];



}
- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id<NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation{

    NSString *fileURL = [[NSURL URLFromPasteboard:[info draggingPasteboard]] path];

    [self sign:fileURL withSHA1Hash:[self sha1HashAtIndex:row]];



    return YES;
}
- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id<NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation{

    if (dropOperation==NSTableViewDropOn) return YES;
    //  NSLog(@"row is %i:%@",row,info);
    return NO;
}
- (void)tableView:(NSTableView *)tableView updateDraggingItemsForDrag:(id<NSDraggingInfo>)draggingInfo{

}
- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender{
    NSLog(@"entered");
    return NSDragOperationCopy;
}
- (void)draggingExited:(id<NSDraggingInfo>)sender{
    NSLog(@"exited");
}
@end
