//
//  TCSCertificateUIProtocol.h
//  Identity Manager
//
//  Created by Timothy Perfitt on 11/18/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TCSCertificateUIProtocol <NSObject>
-(void)showCertificate:(id)certificate;

@end

NS_ASSUME_NONNULL_END
