#import <Cocoa/Cocoa.h>

#import "TCSCertificateUIProtocol.h"
#import <SecurityInterface/SFCertificateTrustPanel.h>
#import "TCSPreferenceController.h"

@interface AppDelegate : NSObject <NSApplicationDelegate,NSMenuDelegate,TCSCertificateUIProtocol>

- (IBAction)showLog:(id)sender ;
-(void)showCertificate:(id)certificate;
-(TCSPreferenceController *)preferenceController;
-(void)showPreferences:(id)sender;
@end

