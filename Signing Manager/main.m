#import <Cocoa/Cocoa.h>
#include <unistd.h>
#import "TCSPreferenceController.h"
#import "TCSIdentityManager.h"
#import "TCSKeychain.h"
#import "TCSConstants.h"
void usage(){

    fprintf(stderr,"Usage:\n");
    fprintf(stderr,"signingmgr -r\n");
    fprintf(stderr,"signingmgr -p\n");

    fprintf(stderr,"signingmgr -s -i <api host> [-d]\n\n");

    fprintf(stderr,"-s                  Save settings.  Requires -i and -a. Optionally use -d.\n");
    fprintf(stderr,"-i <api host url>   Specify host for API signing operations.\n");
    fprintf(stderr,"-a <api_key>        Specify api key for accessing signing service.\n");
    fprintf(stderr,"-u                  trust self-signed certificates.\n");

    fprintf(stderr,"-d                  debug logging\n");
    fprintf(stderr,"-r                  refresh and display available certificates.\n");
    fprintf(stderr,"-p                  print configuation.\n");
    fprintf(stderr,"-h                  this message.\n");

    fprintf(stderr,"\nExamples:\n");
    fprintf(stderr,"---------\n");

    fprintf(stderr,"Setup configuration\n\"/Applications/Signing Manager.app/Contents/MacOS/Signing Manager\" -s -i http://signingservice.local:3000  -a 5612C7D8-89D2-4DB6-ADE4-0E83B7931228 -d\n");
    fprintf(stderr,"---------\n");

    fprintf(stderr,"Print configuration\n\"/Applications/Signing Manager.app/Contents/MacOS/Signing Manager\" -p\n");
    fprintf(stderr,"---------\n");
    fprintf(stderr,"Refresh Certificates\n\"/Applications/Signing Manager.app/Contents/MacOS/Signing Manager\" -r\n\n");


}
int main(int argc,  const char * argv[]) {

    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    if ([arguments indexOfObject:@"-s"]!=NSNotFound ||
        [arguments indexOfObject:@"-r"]!=NSNotFound ||
        [arguments indexOfObject:@"-p"]!=NSNotFound ||
        [arguments indexOfObject:@"-h"]!=NSNotFound){

        int uflag,pflag,aflag,vflag,rflag,sflag,iflag,tflag,dflag,ch;

        if (argc==1){
            usage();
            return 0;

        }
        char *api_key=nil;
        char *api_host=nil;
        uflag=pflag = aflag=vflag = rflag = sflag = iflag = tflag = dflag =0;
        while ((ch = getopt(argc, (char *const *)argv, "ua:si:drvph?")) != -1) {
            switch (ch) {
                case 's':
                    sflag=1;
                    break;
                case 'i':

                    api_host=strndup(optarg, 1024);
                    iflag=1;
                    break;
                case 'a':

                    api_key=strndup(optarg, 1024);
                    aflag=1;
                    break;

                case 'd':
                    dflag=1;
                    break;
                case 'u':
                    uflag=1;
                    break;


                case 'p':
                    pflag = 1;
                    break;

                case 'r':
                    rflag = 1;
                    break;

                case 'v':
                    vflag = 1;
                    break;

                case 'h':
                default:
                    usage();
                }
        }
        argc -= optind;
        argv += optind;

        if (sflag) { //set
            if (!iflag || !aflag){
                fprintf(stderr, "%s\n","You must specify an API host with -i and an api key with -a");
                exit(-1);
            }

            TCSPreferenceController *prefs=[TCSPreferenceController sharedController];
            prefs.apiHost=[NSString stringWithUTF8String:api_host];
            prefs.debug=dflag;
            prefs.trustSelfSigned=uflag;
            [TCSKeychain setPassword:[NSString stringWithUTF8String:api_key] forService:TCSKEYCHAINSERVICE account:TCSKEYCHAINACCOUNT accessGroup:TCSKEYCHAINACCESSGROUP];

        }
        else if (pflag){
            TCSPreferenceController *prefs=[TCSPreferenceController sharedController];

           fprintf(stderr,"api_host: %s\n",prefs.apiHost.UTF8String);
            fprintf(stderr,"trust self signed: %s\n",prefs.trustSelfSigned==YES?"true":"false");

           fprintf(stderr,"debug: %s\n",prefs.debug==YES?"true":"false");
            NSString *apiKey=[TCSKeychain passwordForService:TCSKEYCHAINSERVICE account:TCSKEYCHAINACCOUNT
            accessGroup:TCSKEYCHAINACCESSGROUP error:nil];

            if (!apiKey || apiKey.length==0) {

                fprintf(stderr,"API key: Not Set\n");

            }
            else {
                fprintf(stderr,"API key: Set\n");
            }
        }
        else if (rflag) {
            fprintf(stderr,"Refreshing certificates....\n");
            [[TCSIdentityManager manager] toggleCard:nil];
            [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:2]];

            fprintf(stderr,"Refresh complete. Checking for new certificates....\n");

            NSArray *certs=[[TCSIdentityManager manager] certificates];
            sleep(1);
            int tries=0;
            while (certs.count==0 && tries<30) {
                fprintf(stderr,"no certificates found yet. Retrying.\n");
                sleep(1);
                tries++;
                certs=[[TCSIdentityManager manager] certificates];
            }

            if (certs.count>0) {

                [certs enumerateObjectsUsingBlock:^(NSDictionary *currCert, NSUInteger idx, BOOL * _Nonnull stop) {

                    NSDate *validTo=[currCert objectForKey:@"expireDate"];
                    fprintf(stderr,"------------------------------------------------\n");
                    fprintf(stderr,"Subject: %s\n",[[currCert objectForKey:@"label"] UTF8String]);
                    fprintf(stderr,"SHA1 Hash:%s\n",[[currCert objectForKey:@"hash"] UTF8String]);
                    fprintf(stderr,"Expire Date:%s\n",[[validTo description] UTF8String]);


                }];

            }
            else {
                fprintf(stderr,"No Certificates Found\n");
                return -1;
            }

        }
    }
    else
    {
        return NSApplicationMain(argc, argv);

    }
}

