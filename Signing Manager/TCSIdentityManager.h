//
//  TCSIdentityManager.h
//  Identity Manager
//
//  Created by Timothy Perfitt on 11/18/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSIdentityManager : NSObject
+ (instancetype)manager;
-(BOOL)isInserted:(id)sender;
-(void)insertCard:(id)sender;
-(void)removeCard:(id)sender;
-(void)toggleCard:(__nullable id)sender;
-(NSArray *)certificates;

@end

NS_ASSUME_NONNULL_END
