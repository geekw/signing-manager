#import "TCSKeychain.h"
#import "AppDelegate.h"
#import "TCSTokenStatusMenuWindowController.h"
#import <CryptoTokenKit/CryptoTokenKit.h>
#import "TCSUnifiedLogger.h"
#import "TCSIdentityManager.h"
#import "TCSConstants.h"
#import "TCSKeychain.h"
#import "TCSConstants.h"
@interface AppDelegate ()
@property (strong) NSXPCConnection *connectionToService;
@property (strong) NSString *apiHost;
@property (weak) IBOutlet NSWindow *window;
@property (strong) NSStatusItem *theItem;
@property (strong) IBOutlet NSMenu *theMenu;
@property (weak) NSStatusBar *bar;
@property (strong) NSString *apikey;
@property TCSTokenStatusMenuWindowController *statusMenuWindowController;
@property (weak) IBOutlet NSWindow *preferenceWindow;
@end

@implementation AppDelegate
- (IBAction)showPreferencesButtonPressed:(id)sender {
    [self showPreferences:self];
}
-(void)showPreferences:(id)sender{

    self.apiHost=self.preferenceController.apiHost;

    self.apikey=[TCSKeychain passwordForService:TCSKEYCHAINSERVICE account:TCSKEYCHAINACCOUNT
                        accessGroup:TCSKEYCHAINACCESSGROUP error:nil];

    [NSApp runModalForWindow:self.preferenceWindow];


}
- (IBAction)prefCancelButtonPressed:(id)sender {
    [self.preferenceWindow orderOut:nil];
    [NSApp stopModal];
}
- (IBAction)prefOkButtonPressed:(id)sender {
    [self.preferenceWindow orderOut:nil];
    [NSApp stopModal];
        [TCSKeychain setPassword:self.apikey forService:TCSKEYCHAINSERVICE account:TCSKEYCHAINACCOUNT accessGroup:TCSKEYCHAINACCESSGROUP];
    self.preferenceController.apiHost=self.apiHost;
}
- (IBAction)showLog:(id)sender {

    NSURL *sharedContainerURL=[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:TCSAPPGROUPIDENTIFIER];

    [[NSWorkspace sharedWorkspace] openURL:[sharedContainerURL URLByAppendingPathComponent:@"SigningManager.log"]];
}

- (IBAction)showCertificates:(id)sender {
    if (!self.statusMenuWindowController) {

               self.statusMenuWindowController=[[TCSTokenStatusMenuWindowController alloc] initWithWindowNibName:@"TCSTokenStatusMenuWindowController"];

               self.statusMenuWindowController.certificateInterfaceDelegate=self;

           }

    [self.statusMenuWindowController showCertificateWindow:self];
    
}
- (IBAction)refreshMenuItemSelected:(id)sender {
    [self.statusMenuWindowController refresh:self];
}


-(void)awakeFromNib{

    [self showCertificates:self];

    #ifdef BETA

        NSString *compileDate = [NSString stringWithUTF8String:__DATE__];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MMM d yyyy"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [df setLocale:usLocale];
        NSDate *betaExpireDate = [[df dateFromString:compileDate] dateByAddingTimeInterval:60*60*24*14];

        NSTimeInterval ti=[betaExpireDate timeIntervalSinceNow];
        if (ti<0) {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Beta Period Ended";
            alert.informativeText=@"This beta has expired.  Please visit twocanoes.com to download the release version.";

            [alert addButtonWithTitle:@"Quit"];
            [alert addButtonWithTitle:@"Visit"];
            NSInteger res=[alert runModal];
            if (res==NSAlertSecondButtonReturn) {
                NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"mainwebsite"]];
                [[NSWorkspace sharedWorkspace]  openURL:url];

            };
            [NSApp terminate:self];
        }
        else {


            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"WARNING";
            alert.informativeText=[NSString stringWithFormat:@"THIS IS A PRE-RELEASE SOFTWARE.\n\nThis build will expire on %@",[betaExpireDate description]];

            [alert addButtonWithTitle:@"I Agree"];
            [alert addButtonWithTitle:@"Quit"];
            NSInteger res=[alert runModal];
            if (res==NSAlertSecondButtonReturn) {
                [NSApp terminate:self];

            };
        }
    #endif
    
//    [self activateStatusMenu:self];

}

-(void)showCertificate:(id)certRef{
    [self.theMenu cancelTracking];

    [NSTimer scheduledTimerWithTimeInterval:0.1 repeats:NO block:^(NSTimer * _Nonnull timer) {
        NSArray* certArray = @[ certRef ];
              SecPolicyRef policyRef=SecPolicyCreateBasicX509();

              if (!policyRef) return;
              SecTrustRef trustRef;

              OSStatus status=SecTrustCreateWithCertificates((__bridge CFTypeRef _Nonnull)(certArray), policyRef, &trustRef);


             if(status==errSecSuccess) {

                 [[SFCertificatePanel sharedCertificatePanel] runModalForTrust:trustRef showGroup:NO];

             }
    }];

}
-(void)pop:(NSTimer *)timer {
    [self.theItem popUpStatusItemMenu:self.theMenu];
}

-(void)statusItemClicked:sender {
    [NSApp activateIgnoringOtherApps:YES];
    [self.statusMenuWindowController loadCertificates:self ];
    [NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(pop:) userInfo:nil repeats:NO];


}
- (void)activateStatusMenu:(id)sender
{
    //
    //        [[self.theMenu itemWithTag:22] setView:self.statusMenuWindowController.window.contentView];

            if (self.theItem == nil) {
                self.bar = [NSStatusBar systemStatusBar];

                self.theItem = [self.bar statusItemWithLength:NSVariableStatusItemLength];
                NSImage *image=[NSImage imageNamed:@"shield"];
                  [image setTemplate:YES];
                [self.theItem setImage:image];

                [self.theItem setAction:@selector(statusItemClicked:)];
                [self.theItem setTarget:self];
                [self.theItem setHighlightMode:NO];
        //        [self.theItem setMenu:self.theMenu];
                [self.theItem setHighlightMode:YES];
        //        [self updateInterfaceData];

                // kickoffnotification();

            }

}
 - (void)deactivateStatusMenu:(id)sender
{
    if (self.theItem != nil) {
        [self.bar removeStatusItem:self.theItem];

        self.theItem=nil;
    }
}

-(TCSPreferenceController *)preferenceController{

   return [TCSPreferenceController sharedController];

}
@end
