//
//  TCSIdentityManager.m
//  Identity Manager
//
//  Created by Timothy Perfitt on 11/18/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.
//

#import "TCSIdentityManager.h"
#import "TCSConstants.h"
#import <SecurityInterface/SFCertificateTrustPanel.h>
#import <CryptoTokenKit/CryptoTokenKit.h>
#import "NSData+HexString.h"
#import "NSData+SHA1.h"
#import "TCSKeychain.h"
#import "TCSConstants.h"

@interface TCSIdentityManager ()
@property (nonatomic, strong) TKTokenWatcher *watcher;
@property (assign) BOOL inFlight;
@end

@implementation TCSIdentityManager
+ (instancetype)manager {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];

        [sharedMyManager startObserving];

    });
    return sharedMyManager;
}

-(void)insertCard:(id)sender{
    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:TOUCHCARDPATH]==NO){
        NSError *err;
        if([@"" writeToFile:TOUCHCARDPATH atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){

            NSLog(@"%@",err.localizedDescription);
        }

    }
    NSLog(@"post changed");

    [[NSNotificationCenter defaultCenter] postNotificationName:TCSCARDINSERTIONCHANGED object:self];

}
-(void)removeCard:(id)sender{
    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:TOUCHCARDPATH]==YES){
        [fm removeItemAtPath:TOUCHCARDPATH error:nil];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:TCSCARDINSERTIONCHANGED object:self];

}

-(void)toggleCard:(__nullable id)sender{
    [self removeCard:self];


    NSTimer *timer=[NSTimer timerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [self insertCard:self];
    }];
    NSRunLoop *loop = [NSRunLoop currentRunLoop];
    [loop addTimer:timer forMode:NSRunLoopCommonModes];

}
-(BOOL)isInserted:(id)sender{

    NSFileManager *fm=[NSFileManager defaultManager];
    return [fm fileExistsAtPath:TOUCHCARDPATH];
}

-(void)startObserving{
 /*   [[TKSmartCardSlotManager defaultManager] addObserver:self
             forKeyPath:@"slotNames"
                 options:(NSKeyValueObservingOptionNew |
                          NSKeyValueObservingOptionOld)
                 context:nil];
  */
    self.watcher=[[TKTokenWatcher alloc] init];
    __unsafe_unretained typeof(self) weakSelf = self;
    if (@available(macOS 10.13, *)) {
        [self.watcher setInsertionHandler:^(NSString * _Nonnull tokenID) {

            [weakSelf.watcher addRemovalHandler:^(NSString * _Nonnull tokenID) {

                [[NSNotificationCenter defaultCenter] postNotificationName:TCSSMARTCARDCHANGED object:weakSelf];

            } forTokenID:tokenID];

            [[NSNotificationCenter defaultCenter] postNotificationName:TCSSMARTCARDCHANGED object:weakSelf];
        }];
    } else {

        NSLog(@"setInsertionHandler API not availble");
    }
}


-(NSArray *)certificates{

    NSArray *tokenIdentities=[self keychainCertificatesWithTypeToken:YES];
    NSArray *keychainIdentities=[self keychainCertificatesWithTypeToken:NO];

    NSMutableDictionary *uniqueDict=[NSMutableDictionary dictionary];

    [keychainIdentities enumerateObjectsUsingBlock:^(NSDictionary *currDict, NSUInteger idx, BOOL * _Nonnull stop) {

        [uniqueDict setObject:currDict forKey:[currDict objectForKey:@"hash"]];
    }];
    [tokenIdentities enumerateObjectsUsingBlock:^(NSDictionary *currDict, NSUInteger idx, BOOL * _Nonnull stop) {

          [uniqueDict setObject:currDict forKey:[currDict objectForKey:@"hash"]];
      }];

    return [NSArray arrayWithArray:[uniqueDict allValues]];
}
-(NSArray *)keychainCertificatesWithTypeToken:(BOOL)useTokenType{
    NSMutableArray *resultsArray=[NSMutableArray array];
    OSStatus stat = errSecSuccess;
    CFDictionaryRef query = NULL;
    CFTypeRef result = NULL;
    const void *tokenKeys[] = {
        kSecClass,
        kSecMatchLimit,
        kSecAttrAccessGroup,
        kSecReturnAttributes,
        kSecReturnRef
    };
    const void *tokenValues[] = {
        kSecClassIdentity,
        kSecMatchLimitAll,
        kSecAttrAccessGroupToken,
        kCFBooleanTrue,
        kCFBooleanTrue
    };

    const void *keychainIdentityKeys[] = {
         kSecClass,
         kSecMatchLimit,
         kSecReturnAttributes,
         kSecReturnRef
     };
     const void *keychainIdentityValues[] = {
         kSecClassIdentity,
         kSecMatchLimitAll,
         kCFBooleanTrue,
         kCFBooleanTrue
     };

    if (useTokenType==YES){
        query = CFDictionaryCreate(kCFAllocatorDefault,
                                   tokenKeys,
                                   tokenValues,
                                   sizeof(tokenValues) / sizeof(tokenValues[0]),
                                   &kCFTypeDictionaryKeyCallBacks,
                                   &kCFTypeDictionaryValueCallBacks);
    }
    else {
        query = CFDictionaryCreate(kCFAllocatorDefault,
                                   keychainIdentityKeys,
                                   keychainIdentityValues,
                                   sizeof(keychainIdentityValues) / sizeof(keychainIdentityValues[0]),
                                   &kCFTypeDictionaryKeyCallBacks,
                                   &kCFTypeDictionaryValueCallBacks);
    }
    if (!query) {
        NSLog(@"error creating dict");
        goto cleanup;
    }
    
    stat = SecItemCopyMatching(query, (CFTypeRef *)&result);
    if(stat) {
        if (stat != errSecItemNotFound) {
            NSLog(@"keychain error %i when getting certificates",stat);
        }
        else {
            NSLog(@"No certificates were found.");
        }
        goto cleanup;
    }

    if (CFGetTypeID(result) == CFArrayGetTypeID()) {
        NSArray *certInfoArray=(__bridge NSArray *)result;
        NSLog(@"%@",result);

        [certInfoArray enumerateObjectsUsingBlock:^(NSDictionary *certInfo, NSUInteger idx, BOOL * _Nonnull stop) {
            SecCertificateRef cert;

            SecIdentityRef identity=nil;
            NSString *label;
//            NSString *expireDate;
            
            if ((label=[certInfo objectForKey:@"labl"]) && (identity=(__bridge SecIdentityRef)[certInfo objectForKey:@"v_Ref"])){



                if(SecIdentityCopyCertificate(identity,&cert )!=noErr){
                    NSLog(@"error creating certificate");
                    return;
                }
                SecTrustRef trustRef;
                SecPolicyRef myPolicy = SecPolicyCreateBasicX509();

                SecTrustCreateWithCertificates (cert,myPolicy,&trustRef);

                SecTrustResultType secTrustResult;
                SecTrustEvaluate (trustRef,&secTrustResult);


                NSColor *color=[NSColor textColor];
                NSString *trustString=@"Trusted";
                //TODO: don't need to check for each cert, just leaf?
                if (secTrustResult==kSecTrustResultInvalid ||
                    secTrustResult==kSecTrustResultDeny ||
                    secTrustResult==kSecTrustResultFatalTrustFailure ||
                    secTrustResult==kSecTrustResultOtherError ||
                    secTrustResult==kSecTrustResultRecoverableTrustFailure) {
                    trustString=@"Untrusted";
                    color=[NSColor redColor];

                }

                CFErrorRef err;
                NSDictionary *certValues=(NSDictionary *)CFBridgingRelease(SecCertificateCopyValues(cert, (CFArrayRef)@[(NSString *)kSecOIDX509V1ValidityNotAfter], &err));
                NSDate *validTo;
                if (certValues) {
                    NSDictionary *notAfterDict=[certValues objectForKey:(NSString *)kSecOIDX509V1ValidityNotAfter];

                    if (notAfterDict.count>0){
                        validTo=[NSDate dateWithTimeIntervalSinceReferenceDate:[[notAfterDict objectForKey:@"value"] doubleValue]];
                    }
                }



                NSData *certData=(NSData *)CFBridgingRelease(SecCertificateCopyData(cert));


                NSData *hash=[certData sha1];
                NSString *hashString=[hash hexString];

                NSString *signingGUID=@"";
                NSString *keyGUID=@"";
                if ([label containsString:@"|"]){

                    NSArray *components=[label componentsSeparatedByString:@"|"];

                    if (components.count==3){
                        label=components[0];
                        signingGUID=components[1];
                        keyGUID=components[2];
                    }

                }
                if ([validTo timeIntervalSinceNow]<0) {
                    color=[NSColor redColor];
                    trustString=@"Expired";
                }
                NSString *source=@"Keychain";
                if (useTokenType==YES) source=[NSString stringWithFormat:@"Token (%@)",[certInfo objectForKey:@"tkid"]];
                [resultsArray addObject:@{@"label":label,@"trust":trustString,@"source":source,@"keyGUID":keyGUID,@"signingGUID":signingGUID,@"textColor":color,@"hash":hashString,@"expireDate":validTo,@"certificate":(id)CFBridgingRelease(cert)}];
            }
        }];

    } else {
//        keychain_ctk_list_item(result);
    }

cleanup:
    if(query) {
        CFRelease(query);
    }

    if(result) {
        CFRelease(result);
    }
//    return @[];

    return [NSArray arrayWithArray:resultsArray];
}


@end
