//
//  TCSPreferenceController.h
//  Identity Manager
//
//  Created by Timothy Perfitt on 1/30/20.
//  Copyright © 2020 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSPreferenceController : NSObject
@property (strong) NSString *authHost;
@property (strong) NSString *apiHost;
@property (assign) BOOL debug;
@property (assign) BOOL trustSelfSigned;
@property (assign) BOOL apiUseGet;
@property (assign) BOOL apiSendEncodedSignature;
+ (instancetype)sharedController ;
-(void)readPreferences:(id)sender;

-(void)savePreferences:(id)sender;
@end

NS_ASSUME_NONNULL_END
