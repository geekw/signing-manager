//
//  TCSPreferenceController.m
//  Identity Manager
//
//  Created by Timothy Perfitt on 1/30/20.
//  Copyright © 2020 Twocanoes Software, Inc. All rights reserved.
//

#import "TCSPreferenceController.h"
#import "TCSConstants.h"
@implementation TCSPreferenceController

+ (instancetype)sharedController {
    static TCSPreferenceController * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];
        [sharedMyManager readPreferences:self];


        [sharedMyManager addObserver:sharedMyManager forKeyPath:@"authHost" options:(NSKeyValueObservingOptionNew |NSKeyValueObservingOptionOld) context:nil];

        [sharedMyManager addObserver:sharedMyManager forKeyPath:@"apiHost" options:(NSKeyValueObservingOptionNew |NSKeyValueObservingOptionOld) context:nil];

        [sharedMyManager addObserver:sharedMyManager forKeyPath:@"debug" options:(NSKeyValueObservingOptionNew |NSKeyValueObservingOptionOld) context:nil];

        [sharedMyManager addObserver:sharedMyManager forKeyPath:@"trustSelfSigned" options:(NSKeyValueObservingOptionNew |NSKeyValueObservingOptionOld) context:nil];

        [sharedMyManager addObserver:sharedMyManager forKeyPath:@"apiUseGet" options:(NSKeyValueObservingOptionNew |NSKeyValueObservingOptionOld) context:nil];

        [sharedMyManager addObserver:sharedMyManager forKeyPath:@"apiSendEncodedSignature" options:(NSKeyValueObservingOptionNew |NSKeyValueObservingOptionOld) context:nil];


    });

    return sharedMyManager;
}
- (void)observeValueForKeyPath:(NSString *)keyPath
ofObject:(id)object
  change:(NSDictionary *)change
                       context:(void *)context {

    [self savePreferences:self];

}
-(void)savePreferences:(id)sender{

    NSUserDefaults *mySharedDefaults = [[NSUserDefaults alloc] initWithSuiteName: TCSAPPGROUPIDENTIFIER];

    [mySharedDefaults setObject:self.authHost forKey:@"authHost"];
    [mySharedDefaults setObject:self.apiHost forKey:@"apiHost"];
    [mySharedDefaults setBool:self.debug forKey:@"debug"];
    [mySharedDefaults setBool:self.apiUseGet forKey:@"apiUseGet"];
    [mySharedDefaults setBool:self.apiSendEncodedSignature forKey:@"apiSendEncodedSignature"];

    [mySharedDefaults setBool:self.trustSelfSigned forKey:@"trustSelfSigned"];

}
-(void)readPreferences:(id)sender{
    NSUserDefaults *mySharedDefaults = [[NSUserDefaults alloc] initWithSuiteName: TCSAPPGROUPIDENTIFIER];
    if ([mySharedDefaults objectForKey:@"authHost"] && ![self.authHost isEqualToString:[mySharedDefaults objectForKey:@"authHost"]]) {
        self.authHost=[mySharedDefaults objectForKey:@"authHost"];
    }
    if ([mySharedDefaults objectForKey:@"apiHost"] && ![self.apiHost isEqualToString:[mySharedDefaults objectForKey:@"apiHost"]]) {
        self.apiHost=[mySharedDefaults objectForKey:@"apiHost"];
    }
    if ([mySharedDefaults objectForKey:@"debug"] && self.debug != [[mySharedDefaults objectForKey:@"debug"] boolValue]) {
        self.debug=[mySharedDefaults boolForKey:@"debug"];
    }
    if ([mySharedDefaults objectForKey:@"trustSelfSigned"] && self.trustSelfSigned != [[mySharedDefaults objectForKey:@"trustSelfSigned"] boolValue]) {
        self.trustSelfSigned=[mySharedDefaults boolForKey:@"trustSelfSigned"];
    }

//    else self.debug=NO;
    if ([mySharedDefaults objectForKey:@"apiUseGet"]  && self.apiUseGet != [[mySharedDefaults objectForKey:@"apiUseGet"] boolValue]) {
        self.apiUseGet=[mySharedDefaults boolForKey:@"apiUseGet"];
    }
//    else self.apiUseGet=NO;
    if ([mySharedDefaults objectForKey:@"apiSendEncodedSignature"] && self.apiSendEncodedSignature != [[mySharedDefaults objectForKey:@"apiSendEncodedSignature"] boolValue]) {
        self.apiSendEncodedSignature=[mySharedDefaults boolForKey:@"apiSendEncodedSignature"];
    }
//    else self.apiSendEncodedSignature=NO;
}
-(NSString *)description{

    return [NSString stringWithFormat:@"authHost: %@\napiHost: %@\ndebug: %@\napiUseGet: %@\napiSendEncodedSignature: %@\ntrust Self signed:%@",self.authHost,self.apiHost,self.debug==YES?@"Yes":@"No",self.apiUseGet==YES?@"Yes":@"No",self.apiSendEncodedSignature==YES?@"Yes":@"No",
            self.trustSelfSigned==YES?@"Yes":@"No"];


}
@end
