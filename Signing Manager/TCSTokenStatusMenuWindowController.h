//
//  TCSTokenStatusMenuWindowController.h
//  Identity Manager
//
//  Created by Timothy Perfitt on 11/18/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSCertificateUIProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface TCSTokenStatusMenuWindowController : NSWindowController <NSTableViewDelegate>
-(IBAction)refresh:(id)sender ;
-(IBAction)showCurrentlySelectedCertificate:(id)sender;
-(void)loadCertificates:(id)sender;
-(void)showCertificateWindow:(id)sender;
@property (weak) id <TCSCertificateUIProtocol> certificateInterfaceDelegate;

@end

NS_ASSUME_NONNULL_END
