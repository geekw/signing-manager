#import <os/log.h>
#import <Foundation/Foundation.h>
#import <CryptoTokenKit/CryptoTokenKit.h>
#import "Token.h"
#import "NSData+SHA1.h"
#import "TCSKeychain.h"
#import "TCSConstants.h"
#import "TCSPreferenceController.h"
#import "TCSUnifiedLogger.h"
#import "NSData+HexString.h"

@implementation PIVTokenKeychainKey

- (instancetype)initWithCertificate:(SecCertificateRef)certificateRef objectID:(TKTokenObjectID)objectID certificateID:(TKTokenObjectID)certificateID alwaysAuthenticate:(BOOL)alwaysAuthenticate {
    if (self = [super initWithCertificate:certificateRef objectID:objectID]) {
        _certificateID = certificateID;
        _alwaysAuthenticate = alwaysAuthenticate;
    }
    return self;
}

@end

@implementation TKTokenKeychainItem(PIVDataFormat)

- (void)setName:(NSString *)name {
    if (self.label != nil) {
        self.label = name;

        //        self.label = [NSString stringWithFormat:@"%@ (%@)", name, self.label];
    } else {
        self.label = name;
    }
    
}

@end

@implementation TKSmartCard(PIVDataFormat)







@end

@interface PIVToken()
@end
@implementation PIVToken


-(void)updateAvailableCertificatesInto:(NSMutableArray<TKTokenKeychainItem *> *)items{
    TKTokenObjectID certificateID = [TKBERTLVRecord dataForTag:0x5fc10a];

    TCSPreferenceController *prefController=[TCSPreferenceController sharedController];
    [prefController readPreferences:self];
    NSError *err;

    if (!self.tokenCertificates) {
        self.tokenCertificates=[NSMutableDictionary dictionary];
    }
    [self.tokenCertificates removeAllObjects];

    NSURL* scriptsDirURL = [[NSFileManager defaultManager] URLForDirectory: NSApplicationScriptsDirectory inDomain: NSUserDomainMask appropriateForURL: nil create: YES error: &err];

    NSURL *scriptURL=[scriptsDirURL URLByAppendingPathComponent:@"token"];

    if (prefController.debug==YES) NSLog(@"script path is %@",scriptURL.path);
    NSUserUnixTask *unixTask=[[NSUserUnixTask alloc] initWithURL:scriptURL error:&err];

    NSPipe *outPipe=[NSPipe pipe];
    NSPipe *inPipe=[NSPipe pipe];

    
   NSString *key=[TCSKeychain passwordForService:TCSKEYCHAINSERVICE account:TCSKEYCHAINACCOUNT accessGroup:TCSKEYCHAINACCESSGROUP error:&err ];
    if (!key || key.length==0){
        NSLog(@"No API key found in keychain");
        return ;
    }
    NSData * pwData=[key dataUsingEncoding:NSUTF8StringEncoding];
    unixTask.standardInput=inPipe.fileHandleForReading;
    unixTask.standardOutput=outPipe.fileHandleForWriting;

    if (prefController.debug==YES) NSLog(@"Running script on host %@",prefController.apiHost);
    NSArray *args=@[@"-i",prefController.apiHost,@"-x"];
    if (prefController.trustSelfSigned==YES){
        args=[args arrayByAddingObject:@"-t"];
    }
    [unixTask executeWithArguments:args completionHandler:^(NSError * _Nullable error) {
        if (error!=nil) {
            NSLog(@"There was an error reading the certificate: %@",error.localizedDescription);
        }
    }];

    [inPipe.fileHandleForWriting writeData:pwData];
    [inPipe.fileHandleForWriting closeFile];

    NSData *outputData=[outPipe.fileHandleForReading readDataToEndOfFile];
    if (!outputData) return;

    NSArray *certArray=[NSJSONSerialization JSONObjectWithData:outputData options:0 error:&err];

    [certArray enumerateObjectsUsingBlock:^(NSDictionary *certInfo, NSUInteger idx, BOOL * _Nonnull stop) {
        if (!certInfo) {
            if (prefController.debug==YES) NSLog(@"blank certificate info. skipping");
            return;
        }
        NSString *certDataString=[certInfo objectForKey:@"certificate"];
        if (!certDataString) {
            if (prefController.debug==YES) NSLog(@"blank certificate data. skipping");
            return;
        }

        NSRange startRange=[certDataString rangeOfString:@"-----BEGIN CERTIFICATE-----"];
        NSRange endRange=[certDataString rangeOfString:@"-----END CERTIFICATE-----"];

        if (startRange.location==NSNotFound || endRange.location==NSNotFound) {
            NSLog(@"Invalid PEM certificate. Header not found");
            return;
        }

        unsigned long startPos=startRange.location+startRange.length;
        NSRange pemRange=NSMakeRange(startPos, endRange.location-startPos);
        NSString *pemDataString=[certDataString substringWithRange:pemRange];

        NSData *certData = [[NSData alloc]
                            initWithBase64EncodedString:pemDataString options:NSDataBase64DecodingIgnoreUnknownCharacters];

        if (certData){
            if (prefController.debug==YES) NSLog(@"Populating certificate item with data");

            // Create certificate item.
            SecCertificateRef certificate = SecCertificateCreateWithData(kCFAllocatorDefault, (CFDataRef)certData);
            if (certificate == NULL) {
                return;
            }
            NSData *certificateSHA1Hash=[certData sha1];
            if (prefController.debug==YES) {
                NSLog(@"certificate sha1 hash: %@",[certificateSHA1Hash hexString]);

            }
            TKTokenKeychainCertificate *certificateItem = [[TKTokenKeychainCertificate alloc] initWithCertificate:certificate objectID:certificateID];
            if (certificateItem == nil) {
                CFRelease(certificate);
                return;
            }
            CFStringRef cn;
            if (SecCertificateCopyCommonName(certificate,&cn) !=errSecSuccess) {
                CFRelease(certificate);
                return;
            }
            if (prefController.debug==YES) {
                NSLog(@"certificate cn: %@",cn);

            }

            TKTokenKeychainKey *keyItem = [[PIVTokenKeychainKey alloc] initWithCertificate:certificate objectID:certificateSHA1Hash certificateID:certificateItem.objectID alwaysAuthenticate:NO];

            CFRelease(certificate);

            if (![self.tokenCertificates objectForKey:certificateSHA1Hash]) {
                certificateItem.label=[NSString stringWithFormat:@"%@|%@|%@",cn,[certInfo objectForKey:@"key_guid"],[certInfo objectForKey:@"signing_guid"]];

                if (prefController.debug==YES) {
                    NSLog(@"certificate label: %@",certificateItem.label);

                }

                // Create key item.
                if (keyItem == nil) {
                    return ;
                }
                [keyItem setName:[NSString stringWithFormat:@"key-%li",idx]];
                NSMutableDictionary<NSNumber *, TKTokenOperationConstraint> *constraints = [NSMutableDictionary dictionary];
                keyItem.canSign = YES;
                keyItem.suitableForLogin = NO;
                TKTokenOperationConstraint constraint = PIVConstraintPIN;
                constraints[@(TKTokenOperationSignData)] = constraint;

                if ([keyItem.keyType isEqual:(id)kSecAttrKeyTypeRSA]) {
                    keyItem.canDecrypt = YES;
                    if (YES) {
                        constraints[@(TKTokenOperationDecryptData)] = constraint;
                    }
                } else if ([keyItem.keyType isEqual:(id)kSecAttrKeyTypeECSECPrimeRandom]) {
                    keyItem.canPerformKeyExchange = NO;
                }
                keyItem.constraints = constraints;
                [items addObject:certificateItem];
                [items addObject:keyItem];
                [self.tokenCertificates setObject:certData forKey:certificateSHA1Hash];

            }

        }
    }];

}

- (nullable instancetype)initWithSmartCard:(TKSmartCard *)smartCard AID:(nullable NSData *)AID PIVDriver:(PIVTokenDriver *)tokenDriver error:(NSError **)error {
    TCSPreferenceController *prefController=[TCSPreferenceController sharedController];

    if (prefController.debug==YES) NSLog(@"Initializing SmartCard Driver");
    // Find Card GUID in CHUID.
    __block NSString *instanceID=@"EBB1F74647B54299A857E298CBEEEAF9";

    if (instanceID == nil || instanceID.length == 0) {
        if (error) {
            *error = [NSError errorWithDomain:TKErrorDomain code:TKErrorCodeObjectNotFound userInfo:nil];
            os_log_error(OS_LOG_DEFAULT, "CHUID record does not contain Card GUID");
        }
        return nil;
    }

    if (self = [super initWithSmartCard:smartCard AID:AID instanceID:instanceID tokenDriver:tokenDriver]) {
        // Find out how many on-card key history objects are present, use Key History object.

        // Prepare array with keychain items representing on card objects.
        NSMutableArray<TKTokenKeychainItem *> *items = [NSMutableArray array];
        if (prefController.debug==YES) NSLog(@"Updating Certificates");
        [self updateAvailableCertificatesInto:items];

        if (items.count==0) {
            NSLog(@"No Certificates found");
            return nil;
        }
        else {
            if (prefController.debug==YES) NSLog(@"Found %li Certificates", items.count/2); //have to divide by 2 since both TKTokenKeychainCertificate and PIVTokenKeychainKey are returned.
        }

        if (prefController.debug==YES) NSLog(@"Populating keychain with certificates");
        [self.keychainContents fillWithItems:items];
    }

    return self;
}

- (TKTokenSession *)token:(TKToken *)token createSessionWithError:(NSError * _Nullable __autoreleasing *)error {
    return [[PIVTokenSession alloc] initWithToken:self];
}

@end
