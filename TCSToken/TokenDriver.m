#import <Foundation/Foundation.h>
#import <CryptoTokenKit/CryptoTokenKit.h>

#import "Token.h"

@implementation PIVTokenDriver

- (TKSmartCardToken *)tokenDriver:(TKSmartCardTokenDriver *)driver createTokenForSmartCard:(TKSmartCard *)smartCard AID:(NSData *)AID error:(NSError * _Nullable __autoreleasing *)error {

    TKSmartCardToken *returnVal=[[PIVToken alloc] initWithSmartCard:smartCard AID:AID PIVDriver:self error:error];
    return returnVal;
}

@end
