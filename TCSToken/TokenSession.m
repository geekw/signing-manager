#import <os/log.h>
#import <Foundation/Foundation.h>
#import <CryptoTokenKit/CryptoTokenKit.h>
#import "Token.h"
#import "TokenSession.h"
#import "TCSKeychain.h"
#import "NSData+SHA1.h"
#import "NSData+HexString.h"
#import "TCSPreferenceController.h"
#import "TCSUnifiedLogger.h"
#import "TCSConstants.h"
@implementation PIVAuthOperation

- (instancetype)initWithSession:(PIVTokenSession *)session {
    if (self = [super init]) {
        _session = session;

        self.smartCard = session.smartCard;
        const UInt8 template[] = {self.session.smartCard.cla, 0x20, 0x00, 0x80, 0x08, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
        self.APDUTemplate = [NSData dataWithBytes:template length:sizeof(template)];
        self.PINFormat = [[TKSmartCardPINFormat alloc] init];
        self.PINFormat.PINBitOffset = 5 * 8;
    }

    return self;
}


@end

@implementation PIVTokenSession

- (instancetype)initWithToken:(PIVToken *)token {
        return [super initWithToken:token];
}   

- (TKTokenAuthOperation *)tokenSession:(TKTokenSession *)session beginAuthForOperation:(TKTokenOperation)operation constraint:(TKTokenOperationConstraint)constraint error:(NSError * _Nullable __autoreleasing *)error {
    if (![constraint isEqual:PIVConstraintPIN] && ![constraint isEqual:PIVConstraintPINAlways]) {
        if (error != nil) {
            *error = [NSError errorWithDomain:TKErrorDomain code:TKErrorCodeBadParameter userInfo:@{NSLocalizedDescriptionKey: NSLocalizedString(@"WRONG_CONSTR", nil)}];
        }
        return nil;
    }

    return [[PIVAuthOperation alloc] initWithSession:self];
}

- (BOOL)tokenSession:(TKTokenSession *)session supportsOperation:(TKTokenOperation)operation usingKey:(TKTokenObjectID)keyObjectID algorithm:(TKTokenKeyAlgorithm *)algorithm {
    PIVTokenKeychainKey *keyItem = (PIVTokenKeychainKey *)[self.token.keychainContents keyForObjectID:keyObjectID error:nil];
    if (keyItem == nil) {
        return NO;
    }

    switch (operation) {
        case TKTokenOperationSignData:
            if (keyItem.canSign) {
                if ([keyItem.keyType isEqual:(id)kSecAttrKeyTypeRSA]) {
                    // We support only RAW data format and PKCS1 padding.  Once SecKey gets support for PSS padding,
                    // we should add it here.
                    return [algorithm isAlgorithm:kSecKeyAlgorithmRSASignatureRaw] &&
                    [algorithm supportsAlgorithm:kSecKeyAlgorithmRSASignatureDigestPKCS1v15Raw];
                } else if ([keyItem.keyType isEqual:(id)kSecAttrKeyTypeECSECPrimeRandom]) {
                    if (keyItem.keySizeInBits == 256) {
                        return [algorithm isAlgorithm:kSecKeyAlgorithmECDSASignatureDigestX962SHA256];
                    } else if (keyItem.keySizeInBits == 384) {
                        return [algorithm isAlgorithm:kSecKeyAlgorithmECDSASignatureDigestX962SHA384];
                    }
                }
            }
            break;
        case TKTokenOperationDecryptData:
            if (keyItem.canDecrypt && [keyItem.keyType isEqual:(id)kSecAttrKeyTypeRSA]) {
                return [algorithm isAlgorithm:kSecKeyAlgorithmRSAEncryptionRaw];
            }
            break;
        case TKTokenOperationPerformKeyExchange:
            if (keyItem.canPerformKeyExchange && [keyItem.keyType isEqual:(id)kSecAttrKeyTypeECSECPrimeRandom]) {
                // For NIST p256 and p384, there is no difference between standard and cofactor variants, so answer that both of them are supported.
                return [algorithm isAlgorithm:kSecKeyAlgorithmECDHKeyExchangeStandard] || [algorithm isAlgorithm:kSecKeyAlgorithmECDHKeyExchangeCofactor];
            }
            break;
        default:
            break;
    }
    return NO;
}

- (PIVTokenKeychainKey *)authenticatedKeyForObjectID:(TKTokenObjectID)keyObjectID error:(NSError **)error {
    // Check for authentication status.
    PIVTokenKeychainKey *keyItem = (PIVTokenKeychainKey *)[self.token.keychainContents keyForObjectID:keyObjectID error:error];
    if (keyItem == nil) {
        return nil;
    }
    return keyItem;
}



- (NSData *)tokenSession:(TKTokenSession *)session signData:(NSData *)dataToSign usingKey:(TKTokenObjectID)keyObjectID algorithm:(TKTokenKeyAlgorithm *)algorithm error:(NSError * _Nullable __autoreleasing *)error {


    TCSPreferenceController *prefController=[TCSPreferenceController sharedController];

    if (prefController.debug==YES) NSLog(@"Signing operation requested");

    NSError *err;

    PIVToken *currToken=(PIVToken *)session.token;

    NSDictionary *currentCerts=[currToken tokenCertificates];
    if (!currentCerts) {
        NSLog(@"No certificates available. Aborting signing operation.");
        return nil;
    }

    NSData *currCertData=[currentCerts objectForKey:keyObjectID];
    if (!currCertData) {
        NSLog(@"No certificates found for key ID: %@. Aborting signing operation.",keyObjectID);
        return nil;
    }

    NSData *certificateSHA1Hash=[currCertData sha1];

    NSURL* scriptsDirURL = [[NSFileManager defaultManager] URLForDirectory: NSApplicationScriptsDirectory inDomain: NSUserDomainMask appropriateForURL: nil create: YES error: &err];

    NSURL *scriptURL=[scriptsDirURL URLByAppendingPathComponent:@"token"];

    if (prefController.debug==YES) NSLog(@"Calling script at %@",scriptURL.path);

    NSUserUnixTask *unixTask=[[NSUserUnixTask alloc] initWithURL:scriptURL error:&err];

    NSPipe *outPipe=[NSPipe pipe];
    NSPipe *inPipe=[NSPipe pipe];

    NSString *key=[TCSKeychain passwordForService:TCSKEYCHAINSERVICE account:TCSKEYCHAINACCOUNT accessGroup:TCSKEYCHAINACCESSGROUP error:nil ];
    if (!key || key.length==0){
        NSLog(@"There was an error getting the password from the keychain: %@",err.localizedDescription);
        return nil;
    }
    NSData * pwData=[key dataUsingEncoding:NSUTF8StringEncoding];
    unixTask.standardInput=inPipe.fileHandleForReading;
    unixTask.standardOutput=outPipe.fileHandleForWriting;

    NSData *signatureData=[dataToSign base64EncodedDataWithOptions:0];

    NSString *signatureString=[[NSString alloc] initWithData:signatureData encoding:NSUTF8StringEncoding];


    NSArray *args=@[@"-i",prefController.apiHost,@"-s",@"-a",signatureString,@"-f",certificateSHA1Hash.hexString];
//    if (prefController.debug) args=[args arrayByAddingObject:@"-d"];
    if (prefController.trustSelfSigned==YES){
        args=[args arrayByAddingObject:@"-t"];
    }


    [unixTask executeWithArguments:args completionHandler:^(NSError * _Nullable error) {
        if (error!=nil)  {
            NSLog(@"There was an error reading the signature: %@", error.localizedDescription);
        }
    }];

    [inPipe.fileHandleForWriting writeData:pwData];
    [inPipe.fileHandleForWriting closeFile];

    NSData *outputData=[outPipe.fileHandleForReading readDataToEndOfFile];
    if (!outputData || outputData.length<2) {
        NSLog(@"No data was returned. Check network connectivity and try again. Aborting signing operation.");
        return nil;
    }

    NSDictionary *signatureInfo=[NSJSONSerialization JSONObjectWithData:outputData options:0 error:&err];


    NSString *base64String=signatureInfo[@"signature"];

    NSData *signature = [[NSData alloc]
    initWithBase64EncodedString:base64String options:NSDataBase64DecodingIgnoreUnknownCharacters];
//if can't decode, then error and return nil;
    if (!signature || signature.length==0) {
        NSLog(@"There was an error converting data to base64. Aborting. Data: %@",base64String);
        return nil;
    }
    if (prefController.debug==YES) NSLog(@"Signature successful.");

    return signature;
}


@end
