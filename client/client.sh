#!/bin/bash

options=$(getopt i:f:xta:s $*)
[ $? -eq 0 ] || { 
	echo "Incorrect options provided"
	exit 1
}
eval set -- "$options"
for i
do
	case "$1" in
	-f)
		FINGERPRINT=$2;
		shift; shift;
		;;
	-t)
		TRUSTSELFSIGNED=1;
		shift; shift;
		;;
	-x)
		SHOWCERTS=1
		shift;
		;;
	-a)
		HASH=$2
		shift; shift;
		;;
	-i)
		API_HOST=$2
		shift; shift;
		;;
	-s)
		SIGN=1
		shift;
		;;
	--)
		shift
		break
		;;
	esac

done

read -t 1 api_key

if [ -z "${api_key}" ]; then
	echo please provide an api key via stdin > /dev/stderr
fi

TRUSTFLAG=""
if [ -n "${TRUSTSELFSIGNED}" ] ; then
	TRUSTFLAG="-k"
fi


if [ "${SHOWCERTS}" ]; then
	echo showing certs >&2
	a=$(curl "${TRUSTFLAG}" -H "API_KEY: ${api_key}" "${API_HOST}/signing_certificates" )
	echo $a
elif [ "${SIGN}" ]; then
	if [ -z "${FINGERPRINT}" ] || [ -z "${HASH}" ] || [ -z "${API_HOST}" ]; then
		echo you must provide a api host, hash and a fingerprint with the -f, -a, and -i options
		exit -1
	fi
	curl  "${TRUSTFLAG}" -H "API_KEY: ${api_key}" -H "Content-Type: application/json" -d "{\"blob_to_sign\":\"${HASH}\",\"fingerprint\":\"${FINGERPRINT}\"}" "${API_HOST}"/sign
	
fi


exit 0;