//
//  main.m
//  sign
//
//  Created by Timothy Perfitt on 11/13/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>
#import "TCSKeychain.h"
#import "TCSUnifiedLogger.h"
#import "NSData+HexString.h"
const char *sign_data(NSData *hash, NSString *hashString);
void usage(void);

void find_identity(NSData *inData, SecIdentityRef *identity);

int main(int argc, char * argv[]) {
    @autoreleasepool {
        int f_sign = 0;
        int f_list = 0;
        int f_cert = 0;
        char *fingerprint_string = NULL;
        char *hash_value = NULL;

        int c;

        opterr = 0;

        while ((c = getopt (argc, argv, "sci:h:l")) != -1) {
            switch (c)
            {
                case 's':
                    f_sign = 1;
                    break;
                case 'c':
                    f_cert = 1;
                    break;
                case 'i':
                    fingerprint_string = optarg;
                    break;
                case 'h':
                    hash_value = optarg;
                    break;
                case 'l':
                    f_list = 1;
                    break;
                case '?':
                    if (optopt == 'i')
                        fprintf (stderr, "Option -%i requires an argument.\n", optopt);
                    else if (isprint (optopt))
                        fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                    else
                        fprintf (stderr,
                                 "Unknown option character `\\x%x'.\n",
                                 optopt);
                    usage();
                    return 1;
                default:
                    usage();
                    exit(-1);
            }
        }

        if (f_list==1) {
            NSArray *cns=[TCSKeychain availableIdentityInfo];
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:cns options:0 error:&error];

            NSString *jsonOut=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            fprintf(stdout,"%s\n",jsonOut.UTF8String);


        }
        else if (f_sign==1){
            if (hash_value==nil || fingerprint_string==nil){
                fprintf(stderr,"no hash value, subject value, or algorithm specified. specify with -h <hash value> -i <subject> -a <algorithm>\n");
                usage();
                exit(-1);

            }
            NSData *fingerprint=[NSData dataWithHexString:[NSString stringWithUTF8String:fingerprint_string]];
            const char * signed_data=sign_data(fingerprint, [NSString stringWithUTF8String:hash_value]);

            if (signed_data){

                fprintf(stdout,"%s\n",signed_data);

            }
            else {
                fprintf(stderr,"Error signing data.\n");
                exit(-1);


            }
        }
        else if (f_cert==1) {

            if (fingerprint_string==nil) {
                fprintf(stderr,"no subject name specified. specify with -i <subject name>\n");
                usage();
                exit(-1);
            }
            NSData *data=[NSData dataWithHexString:[NSString stringWithUTF8String:fingerprint_string]];
            if (!data) {
                fprintf(stderr,"Invalid certificate sha1 hash.\n");
                exit(-1);
            }
            SecIdentityRef identity;
            find_identity(data,&identity);

            if (!identity){
                fprintf(stderr,"identity not found.\n");
                exit(-1);

            }
            SecCertificateRef cert;
            SecIdentityCopyCertificate(identity, &cert);

            if (!cert){
                fprintf(stderr,"certificate could not be obtained from identity.\n");
                exit(-1);

            }
            CFRelease(identity);
            NSData *certdata=(NSData *)CFBridgingRelease(SecCertificateCopyData(cert));
            NSData *base64EncodedData=[certdata base64EncodedDataWithOptions:0];

            NSString *base64EncodedString=[[NSString alloc] initWithData:base64EncodedData encoding:NSUTF8StringEncoding];

            fprintf(stdout,"-----BEGIN CERTIFICATE-----\n");

            fprintf(stdout,"%s\n",base64EncodedString.UTF8String);
            fprintf(stdout,"-----END CERTIFICATE-----\n");
        }
        else {
            fprintf(stderr,"no operation specified\n");
            usage();
            exit(-1);


        }

    }
    return 0;
}
void usage(){


    fprintf(stderr,"keychain_sign -s -i <identity sha1 fingerprint> -h <hash>\n");
    fprintf(stderr,"keychain_sign -c -i <identity sha1 fingerprint>\n");
    fprintf(stderr,"keychain_sign -l\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"-s: sign\n");
    fprintf(stderr,"-c: print certificate in base64 format\n");
    fprintf(stderr,"-i <subject name>: subject name of certificate\n");
    fprintf(stderr,"-l: list all available identities\n");


}
const char *sign_data(NSData *hash, NSString *hashString){
    SecIdentityRef identity=nil;
    find_identity(hash,&identity);
    if (!identity){

        fprintf(stderr,"Identity not found or multiple copies found\n");
        exit(-1);
    }

    SecKeyAlgorithm algorithm=kSecKeyAlgorithmRSASignatureRaw;

    NSData *data2sign = [[NSData alloc]
    initWithBase64EncodedString:hashString options:NSDataBase64DecodingIgnoreUnknownCharacters];

    if (!data2sign){
        fprintf(stderr,"Invalid hash to sign. Invalid base64 encoding.\n");
        exit(-1);


    }

    NSData* signature = nil;
    SecKeyRef privateKey;
    SecIdentityCopyPrivateKey(identity, &privateKey);

    CFErrorRef error = NULL;
    signature = (NSData*)CFBridgingRelease(       // ARC takes ownership
                                           SecKeyCreateSignature(privateKey,
                                                                 algorithm,
                                                                 (__bridge CFDataRef)data2sign,
                                                                 &error));
    if (!signature) {
        NSError *err = CFBridgingRelease(error);  // ARC takes ownership

        NSLog(@"error:%@",((NSError *)err).localizedDescription);
        exit(-1);
    }
    CFRelease(identity);
    NSData *base64Signature=[signature base64EncodedDataWithOptions:0];
//    NSString *base64Signature=[signature base64EncodedString];
    NSString *base64SignatureString=[[NSString alloc] initWithData:base64Signature encoding:NSUTF8StringEncoding];
    return base64SignatureString.UTF8String;

}


void find_identity(NSData *inData, SecIdentityRef *identity){

    [TCSKeychain findIdentityWithSHA1Hash:inData returnIdentity:identity];
//    SecIdentityRef test=[TCSKeychain findIdentityWithSHA1Hash:data];
//    SecIdentityRef identity=[TCSKeychain findIdentityWithSubject:inCN];

}

