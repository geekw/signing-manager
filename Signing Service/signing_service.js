var express = require('express');
var https = require('https');
var fs = require('fs');
var bodyParser = require("body-parser");
const NodeRSA = require('node-rsa');
const crypto = require('crypto');
const sha1 = require('sha1');
const uuidv4 = require('uuid/v4');
const { execSync } = require("child_process");

var settings_string = fs.readFileSync('settings.json');
let settings = JSON.parse(settings_string);

const opt = require('node-getopt').create([
  ['p', 'port=[PORT]', 'Set server port', '3000'],
  ['s', 'https', 'Use HTTPS.  Requires providing settings files "server.key" and "server.cert"'],
  ['k', 'keychain', 'Use macOS keychain.  Requires adding keychain_identity_fingerprints to settings.'],
  ['h', 'help', 'Display this help'],
])              // create Getopt instance
  .bindHelp()     // bind option 'help' to default action
  .parseSystem(); // parse command line

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const port = opt.options.p;
const use_keychain = opt.options.k;
  
const fingerprints = (_=> {
  if (use_keychain) {
    return settings.keychain_identity_fingerprints.map(i => i.sha1_hash);
  }
  else { // Use identities supplied directly in settings.json
    const content = settings.identities;
    const calculated_fingerprints = content.map(identity => {
      let key = identity.certificate.replace(/.*-----BEGIN CERTIFICATE-----/, '').trim();
      key = key.replace(/-----END CERTIFICATE-----.*/, '');
      const buffer = Buffer.from(key, 'base64');
      return sha1(buffer);
    });
    return calculated_fingerprints;
  }
})().map(i => i.toLowerCase());

function authorized_fingerprints(api_key) {
  if (use_keychain) {
    const result = settings.keychain_identity_fingerprints.filter(f => {
      return f.api_keys.indexOf(api_key) != -1;
    });
    return result.map(f => f.sha1_hash);
  }
  else {
    const identities = settings.identities;
    let result = [];
    for (let i = 0; i < identities.length; i++) {
      if (!identities[i].api_keys || identities[i].api_keys.indexOf(api_key) != -1) {
        result.push(fingerprints[i]);
      }
    }
    return result;
  }
}

if (opt.options.s) {  // Use https
  try {
    var https_settings = {
      key: fs.readFileSync('server.key'),
      cert: fs.readFileSync('server.cert')
    };
  }
  catch {
    console.error('HTTPS settings could not be loaded from files "server.key" and "server.cert".');
  }

  if (typeof(https_settings) == 'undefined') {
    console.error('Cannot start HTTPS without settings.  Aborting.');
  }
  else {
    https.createServer({
        key: https_settings.key,
        cert: https_settings.cert
    }, app)
    .listen(port, function() {
        console.log(`Twocanoes Secure Signing Service started on port ${port}`);
    });
  }
}
else { // Use http
  app.listen(port, function() {
      console.log(`Twocanoes Signing Service started on port ${port}`)
  });
}

function audit(req, message, type = 'info') {
  const audit_log_path = 'audit.log';
  let content = [];
  content.push(new Date().toLocaleString());
  content.push(req.ip, req.id, req.api_key_name, message, type);
  content = content.map(i => i.replace(',', ' -'));
  content = `${content.join(', ')}\n`;
  fs.appendFile(audit_log_path, content, error => {
    if (error) console.error(error);
  });
}

function authorize(req, res, next) {
  const keys = Object.keys(settings.api_keys);
  const key = req.header('API_KEY');
  const authorized = keys.includes(key);
  const skip_authorization_for_paths = ['/'];
  let audit_message = `Request to path: ${req.path}.`;

  req.api_key_name = settings.api_keys[key] || 'anonymous or unknown';

  if (authorized || skip_authorization_for_paths.includes(req.path)) {
    audit(req, audit_message);
    next();
  }
  else {
    const error_message = "Invalid API key.";
    audit_message += ` ${error_message}`;
    res.status(401).send({error: error_message});
    audit(req, audit_message, 'error');
  }
}

app.all('*', (req, res, next)=> {
  req.id = uuidv4();
  console.log(`${new Date().toLocaleString()} ${req.id}: Request to path: ${req.path}.`);
  authorize(req, res, next);
});

app.get('/', function(req, res) {
  res.send('success');
});

app.post('/sign', (req,res)=> {
  const blob = req.body.blob_to_sign;
  const fingerprint = req.body.fingerprint.toLowerCase();

  if (!blob || !fingerprint) {
    res.status(422).send({error: 'Argument missing.  Required: blob_to_sign, fingerprint'});
    audit(req, 'Argument missing.  Required: blob_to_sign, fingerprint', 'error');
  }
  else {
    const api_key = req.header('API_KEY');
    const identity_index = authorized_fingerprints(api_key).indexOf(fingerprint);
    if (identity_index == -1) {
      const message = 'Invalid fingerprint';
      console.error(message, fingerprint);
      res.status(422).send({error: message});
      audit(req, message);
    }
    else {
      let return_blob;
      if (use_keychain) {
        const command = `./keychain_sign -s -i ${fingerprint} -h ${blob}`;
        try {
          const content = execSync(command).toString().trim();
          return_blob = {"signature": content};
        }
        catch (error) {
          console.error(error.message);
          const message = "Error when signing";
          res.status(500).send({error: message});
          audit(req, message);
          return;
        }
      }
      else {
        const key_data = settings.identities[identity_index].key;
        const pkcs_15_hash = Buffer.from(blob,'base64');
        const signature = crypto.privateEncrypt({key: key_data, padding: crypto.constants.RSA_NO_PADDING}, pkcs_15_hash); 
        return_blob = {"signature": signature.toString('base64')};
      }
      res.send(return_blob);
      audit(req, "Successful signing request");
    }
  }
});

app.get('/signing_certificates', (req,res) => {
  let filtered_identities = [];
  const user_key = req.header('API_KEY');
  if (use_keychain) {
    let command = './keychain_sign -l';
    try {
      const content = execSync(command).toString();
      let identities = JSON.parse(content);
      const filtered_fingerprints = authorized_fingerprints(user_key);
      identities = identities.filter(i => {
        return filtered_fingerprints.indexOf(i.sha1_fingerprint.toLowerCase()) != -1;
      });

      for (let i = 0; i < filtered_fingerprints.length; i++) {
        command = `./keychain_sign -c -i ${filtered_fingerprints[i]}`;
        try {
          const certificate = execSync(command).toString().trim();
          const identity = identities.find(j => {
            return j.sha1_fingerprint.toLowerCase() == filtered_fingerprints[i];
          });
          if (identity) {
            filtered_identities[i] = {
              "cn": identity.cn,
              "certificate": certificate
            }
          }
        }
        catch (error) {
          console.error(error.message);
          const message = `Error requesting certificate for ${filtered_fingerprints[i]}`;
          res.status(500).send({error: message});
          audit(req, message);
          return;
        }
      }
    }
    catch (error) {
      console.error(error.message);
      const message = "Error requesting identities";
      res.status(500).send({error: message});
      audit(req, message);
      return;
    }
  }
  else {
    for (let i = 0; i < settings.identities.length; i++) {
      const allowed_keys = settings.identities[i].api_keys;
      const user_key = req.header('API_KEY');
      if (allowed_keys == undefined || allowed_keys.includes(user_key)) {
        filtered_identities[i] = {
          "cn": settings.identities[i].cn, 
          "certificate": settings.identities[i].certificate
        }
      }
    }
  }
  filtered_identities = filtered_identities.filter(i=>i); // remove empties
  res.send(filtered_identities);
  audit(req, `Identities sent: ${filtered_identities.length}`)
});

